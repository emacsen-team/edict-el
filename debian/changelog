edict-el (1.06-12) unstable; urgency=medium

  * ACK previous NMUs. Thanks, Bastian and Holger!
  * Update Vcs-* and Rules-Requires-Root in debian/control.
  * Source package is now on Salsa.
  * Bug fix: deprecated macro syntax, thanks to <abegui@gmx.com> (Closes:
    #1003781).

 -- Roland Mas <lolando@debian.org>  Tue, 14 Feb 2023 11:34:11 +0100

edict-el (1.06-11.2) unstable; urgency=medium

  * Non-maintainer upload.
  * Convert to 3.0 source format (Closes: #1007363).

 -- Bastian Germann <bage@debian.org>  Tue, 29 Nov 2022 00:36:59 +0100

edict-el (1.06-11.1) unstable; urgency=medium

  * Non maintainer upload by the Reproducible Builds team.
  * No source change upload to rebuild on buildd with .buildinfo files.

 -- Holger Levsen <holger@debian.org>  Tue, 05 Jan 2021 16:34:19 +0100

edict-el (1.06-11) unstable; urgency=medium

  * Upgrade Depends: to handle emacs24, patch from Gabriele Giacone
    <1o5g4r8o@gmail.com> (closes: #754001).
  * Bumped Standards-Version: to 3.9.5 while I'm at it (no changes).

 -- Roland Mas <lolando@debian.org>  Tue, 09 Sep 2014 09:09:18 +0200

edict-el (1.06-10) unstable; urgency=low

  * Disable several dh_auto_* during build (closes: #724052).
  * Updated Standards-Version to 3.9.4 (no changes needed).

 -- Roland Mas <lolando@debian.org>  Mon, 23 Sep 2013 11:46:05 +0200

edict-el (1.06-9) unstable; urgency=low

  * Switched section to lisp.

 -- Roland Mas <lolando@debian.org>  Fri, 19 Nov 2010 11:31:48 +0100

edict-el (1.06-8) unstable; urgency=low

  * Deduplicate search results (closes: #388580).
  * Updated dependency to emacs23 | emacsen.
  * Fixed Lintian warnings and errors.
  * Switched to Debhelper 7.
  * Updated Standards-Version (no changes).

 -- Roland Mas <lolando@debian.org>  Fri, 19 Nov 2010 11:00:21 +0100

edict-el (1.06-7) unstable; urgency=low

  * Replaced dependency on emacs21 | emacsen with emacs22 | emacsen.

 -- Roland Mas <lolando@debian.org>  Sat, 28 Jul 2007 16:54:56 +0200

edict-el (1.06-6) unstable; urgency=low

  * Bumped Standards-Version to 3.6.0 (no changes needed).
  * The edict package is now free and has been moved to the main section,
    hence edict-el can move out of non-free and into main too.
  * Replaced dependency on emacs20 | emacsen with emacs21 | emacsen.

 -- Roland Mas <lolando@debian.org>  Wed, 23 Jul 2003 20:34:02 +0200

edict-el (1.06-5) unstable; urgency=low

  * Bumped Standards-Version to 3.5.7, with necessary modifications.

 -- Roland Mas <lolando@debian.org>  Tue, 10 Sep 2002 13:26:20 +0200

edict-el (1.06-4) unstable; urgency=low

  * Moved to contrib (closes: #98233).

 -- Roland Mas <lolando@debian.org>  Mon, 21 May 2001 12:22:28 +0200

edict-el (1.06-3) unstable; urgency=low

  * Fixed autoloads.

 -- Roland Mas <lolando@debian.org>  Thu, 26 Apr 2001 14:47:02 +0200

edict-el (1.06-2) unstable; urgency=low

  * Removed the keybindings from the startup file, put them in an example
    file instead.
  * Added a small README.Debian file.

 -- Roland Mas <lolando@debian.org>  Tue, 24 Apr 2001 14:23:20 +0200

edict-el (1.06-1) unstable; urgency=low

  * Initial Release.

 -- Roland Mas <lolando@debian.org>  Sat, 21 Apr 2001 22:36:31 +0200

 
